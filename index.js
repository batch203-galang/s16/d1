// console.log("Hello, World!");

// [Section] Arithmetic Operators

	let x = 1397;
	let y = 7831;

	// Addition
	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	// Subtraction
	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	// Multiplication
	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	// Division
	let quotient = x / y;
	console.log("Result of division operator: " + quotient);

	// Modulo - remainder
	let modulo = x % y;
	console.log("Result of modulo operator: " + modulo);


// [SECTION] Assignment Operator 
	// Basic Assignment Operator (=)
	// The assignment operator assigns the value of the "right hand" operand to a variable.
	let assignmentNumber = 8;
	console.log("The current value of the assignmentNumber variable: " + assignmentNumber);

	// Addition Assignment Operator
						// 8 + 2
	// assignmentNumber = assignmentNumber + 2; long method
	assignmentNumber += 2; //short hand method
	console.log("Result of addition assignmentNumber operator: "+ assignmentNumber);

	// Subtraction/Multiplication/Division (-=. *=, /=)
	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator: "+assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator: "+assignmentNumber);

	assignmentNumber /= 2;
	console.log("Result of division assignment operator: "+assignmentNumber);

// [SECTION] PEMDAS (Order of Operations)
// Multiple Operators and Parenthesis
// When multiple operators are applied in a single statement, it follows PEMDAS.

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of MDAS operation: " + mdas);

// let pemdas = (1 + (2 - 3) * (4 / 5); // 0.20
// grouping the operators using parenthesis will deliver different results.
let pemdas = (1 + (2 - 3)) * (4 / 5);
console.log("Result of pemdas operation: "+ pemdas); //0

// [SECTION] Increment and Decrement
// Operators that add or substract a values by 1 and reassign the value of the variable where the increment (++)/ decrement (--) was applied.

	let z = 1;

	let increment = ++z;
	console.log("Result of pre-increment: "+ increment);
	console.log("Result of pre-increment for z: "+ z);

	increment = z++;
	console.log("Result	of post-increment: " +increment);
	console.log("Result of the post-increment for z: "+z);

	let decrement = --z;
	console.log("Result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement for z: " + z);

	let = z--;
	console.log("Result of post-decrement: " + decrement);
	console.log("Result of post-decrement for z: " + z);

// [SECTION] Type Coercion
// is the automatic conversion of values from one data type to another.

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log	(coercion);
console.log(typeof coercion); //typeof to know what type is the variable

let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

/*
	- The result is a number
	-The boolean "true" is also associated with the value of 1.
*/
let numE = true + 1;
console.log(numE);

/*
	- The result is a number
	-The boolean "fals" is also associated with the value of 0.
*/
let numF = false + 1;
console.log(numF);

// [SECTION] Comparison Operators
// Comparison Operators are used to evaluate and compare the left and right operands.
// After evaluation, it returns a boolean value.

let juan = "juan";

// Equality Operator (==)
/*
	- Checks whether the operands are equal/have the same content.
*/
console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == '1'); //true
console.log(0 == false); //true
// compare 2 strings that are the same
console.log('juan' == 'juan'); //true
// compare a string with a variable "juan"
console.log('juan' == juan); //true

// Inequality operator (!=)
/*
	- Checks whether the operands are not equal/have the different value.
	-Attempts to CONVERT AND COMPARE operands of different data type.
*/
console.log(1 != 1); //false
console.log(2 != 1); //true
console.log(0 != false); //false
console.log('juan' != 'juan'); //false
console.log('juan' != juan); //false

// Strict Equality operator (===)
// check whether the operads are equal/have the same content.
// also COMPARES the DATA TYPES of 2 values.

console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 === '1'); //false
console.log(1 === false); //false
console.log('juan' === 'juan');
console.log	('juan' === juan);

// Strict Inequality Operator (!==)
/*
	- check whether the operads are not equal/don't have the same content.
	- also COMPARES the DATA TYPES of 2 values.
*/
console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== '1'); //true
console.log(1 !== false); //true
console.log('juan' !== 'juan'); //false
console.log	('juan' !== juan); //false

// [SECTION] Relational Operators
// Some comparison operators check whether one values is greater or less than to the other value.

let a = 50;
let b = 50;

// GT or Greater Than Operator (>)
let isGreaterThan = a > b;
console.log(isGreaterThan);

// LT or Less Than Operator (<)
let isLessThan = a < b;
console.log(isLessThan);

// GTE or Greater Than or Equal (>=)
let isGTorEqual = a >= b;
console.log(isGTorEqual); //FALSE

// LTE or Less Than or Equal (<=)
let isLTOrEqual = a <= b;
console.log(isLTOrEqual); // TRUE

let numString = "30";
console.log(a > numString);

let str = "twenty"
console.log(b >= str); //false
// Since the string is not numeric, the string was not converted to a number. 65 NaN (Not a Number);

// [SECTION] Logical Operators
// Logical Operators allow us to be more specific in the logical combination of condition and evaluations. It return boolean.

let isLegalAge = true;
let isRegistered = false;

// Logical AND operator (&& - Double Ampersand)
// Returns true if all operands are true.
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of Logical AND Operator: " + allRequirementsMet);
// Logical OR Operator (|| - Double)

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR Operator: " + someRequirementsMet);

// Logical NOT Operator (! - Exclamation Point)
// Returns the opposite value
let someRequirementsNotMet = !isLegalAge;
console.log("Result of Logical NOT Operator: " + someRequirementsNotMet);